export default class LoginRequest{

    constructor({ username = '' , password = '', empresa = false}){

        this.username = username;
        this.password = password;
        this.empresa = empresa;

    }

    validate(){

        if( this.username === '' || this.username === null ) return this.empresa ? 'RUC requerido.' : 'DNI requerido.';
        if(!this.empresa && this.username.length < 8) return 'DNI debe tener 8 dígitos.';
        if( this.empresa && this.username.length < 11) return 'RUC debe tener 11 dígitos.';
        if( this.password === '' || this.password === null ) return 'Contraseña requerida.';

        return null;

    }

}

