export default class LoginRequest{

    constructor({ dni = '' , nombreTrabajador = '', cargo = '', profesion = '', fechaInicio= null, fechaFin= null, idTrabajador = null}){

        this.dni = dni;
        this.nombreTrabajador = nombreTrabajador;
        this.cargo = cargo;
        this.profesion = profesion;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.idTrabajador = idTrabajador;

    }

    validate(){

        if( this.dni === '' || this.dni === null ) return 'DNI requerido.';
        if( this.dni.length < 8) return 'DNI debe tener 8 dígitos.';
        if( this.nombreTrabajador === '' || this.nombreTrabajador === null ) return 'Nombre trabajador necesario.';

        return null;

    }

}

