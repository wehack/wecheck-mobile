export default class SignUpRequest{

    constructor({ numeroDocumento = '' , password = '', nombres = '', empresa = false }){

        this.numeroDocumento = numeroDocumento;
        this.password = password;
        this.nombres = nombres;
        this.empresa = empresa;

    }

    validate(){

        if( this.numeroDocumento === '' || this.numeroDocumento === null ) return this.empresa ? 'RUC requerido.' : 'DNI requerido.';
        if(!this.empresa && this.numeroDocumento.length < 8) return 'DNI debe tener 8 dígitos.';
        if( this.empresa && this.numeroDocumento.length < 11) return 'RUC debe tener 11 dígitos.';
        if( this.password === '' || this.password === null ) return 'Contraseña requerida.';

        return null;

    }

}

