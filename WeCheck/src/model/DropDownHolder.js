const TYPE_SUCCESS = 'success';
const TYPE_ERROR = 'error';
const TYPE_WARN = 'warn';
const TYPE_INFO = 'info';

export default class DropDownHolder {

    static dropDown = null;
    static TYPE_SUCCESS = 'success';
    static TYPE_ERROR = 'error';
    static TYPE_WARN = 'warn';
    static TYPE_INFO = 'info';

    static setDropDown(dropDown) {
        this.dropDown = dropDown
    }

    static showAlert({ type = TYPE_SUCCESS , title = 'Success' , message = 'Success' }) {
        this.dropDown.alertWithType(type, title, message)
    }

}