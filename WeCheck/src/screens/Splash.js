import React from 'react';
import {
    Image,
    StyleSheet,
    AsyncStorage
} from 'react-native';
import {
    Container,
    Grid,
    Row
} from 'native-base';
import {Font } from "expo";
import { Colors, Storage, NavigationC } from '../constants';

export default class Screen extends React.Component {

    constructor(props){
        super(props);
    }

    componentWillMount() {

        Font.loadAsync({

            Roboto: require("native-base/Fonts/Roboto.ttf"),
            Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")

        }).then( () => {

            setTimeout( async () =>{

                let isLogged = await this.isLogged();
                this.props.navigation.navigate(isLogged ? NavigationC.APP: NavigationC.AUTH);

            },1000);

        });



    }

    async isLogged(){

        let token = await AsyncStorage.getItem(Storage.TOKEN);

        return token !== null;

    }

    render() {
        return (
            <Container style = {styles.container}>
                <Grid>
                    <Row style = {{justifyContent: 'center',alignItems:'center'}}>

                    </Row>
                </Grid>
            </Container>
        );
    }

}

const styles = StyleSheet.create({

    container : { backgroundColor : Colors.baseColor }

});
