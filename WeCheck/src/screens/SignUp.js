import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    AsyncStorage,
    View
} from 'react-native';
import {
    Text,
    CheckBox
} from 'native-base';
import { WButton, WInput, WContainer, WFContainer } from  '../components';
import { Colors, Storage, NavigationC } from '../constants';
import { AuthApi, UserApi } from '../api';
import { DropDownHolder , SignUpRequest, LoginRequest } from "../model";
import ServiceConfig from "../constants/Service";

export default class Screen extends React.Component {

    state = {

        empresa : true,
        documento : '',
        nombres : '',
        password : '',
        securePassword : true

    };

    componentWillMount(){

    }

    register(){

        let signUpReq = new SignUpRequest({ numeroDocumento : this.state.documento, password : this.state.password, empresa : this.state.empresa});
        let message = signUpReq.validate();

        if( message !== null ){

            DropDownHolder.showAlert({type : DropDownHolder.TYPE_ERROR , title : 'Valores requeridos' , message});

        }else{

            AuthApi.signUp({
                signUpReq,
                onSuccess : () => this.login(),
                onError : () => {

                    DropDownHolder.showAlert({type : DropDownHolder.TYPE_ERROR , title : 'No se pudo ingresar' , message : 'Usuario no autorizado.'});

                }

            });

        }

    }

    login(){

        let loginReq = new LoginRequest({ username : this.state.documento , password : this.state.password, empresa : this.state.empresa});
        let message = loginReq.validate();

        if(message !== null){

            DropDownHolder.showAlert({type : DropDownHolder.TYPE_ERROR , title : 'Valores requeridos' , message});

        }else{

            AuthApi.signIn({

                loginReq,
                onSuccess : async (res) => {

                    await AsyncStorage.setItem(Storage.TOKEN, res.token );
                    this.props.navigation.navigate(NavigationC.APP)

                },
                onError : () => {

                    DropDownHolder.showAlert({type : DropDownHolder.TYPE_ERROR , title : 'No se pudo ingresar' , message : 'Usuario no autorizado.'});

                }

            });

        }

    }

    render() {
        return (
            <WContainer form>
                <WFContainer>
                    <TouchableOpacity style={{flexDirection:'row'}} onPress={()=> this.setState({empresa : !this.state.empresa}) }>
                        <CheckBox checked={this.state.empresa}
                                  onPress={()=> this.setState({empresa : !this.state.empresa}) }/>
                        <Text style={{marginLeft:20}}>
                            Empresa
                        </Text>
                    </TouchableOpacity>
                    <WInput placeholder={this.state.empresa ? 'RUC' : 'DNI'}
                            value={this.state.documento}
                            keyboardType={'number-pad'}
                            onChangeText={(documento) => this.setState({documento}) }/>
                    <WInput placeholder={'Nombres'}
                            value={this.state.nombres}
                            keyboardType={'number-pad'}
                            onChangeText={(nombres) => this.setState({nombres}) }/>
                    <WInput placeholder={'Contraseña'} iconName={this.state.securePassword?'ios-eye':'ios-eye-off'}
                            value={this.state.password}
                            secureTextEntry={this.state.securePassword}
                            onPressIcon={() => this.setState({securePassword:!this.state.securePassword})}
                            onChangeText={(password) => this.setState({password}) }/>

                    <WButton btnStyle={styles.btnStyle} text={'REGISTRARME'}
                             onPress={this.register.bind(this)}/>

                    <TouchableOpacity style={styles.registerStyle}
                                      onPress={() => this.props.navigation.navigate(NavigationC.AUTH_SIGN_IN) }>
                        <Text style={styles.textPr}>
                            ¿Ya tienes una cuenta?
                        </Text>
                        <Text style={styles.textPb}>
                            Inicia Sesión
                        </Text>
                    </TouchableOpacity>
                </WFContainer>
            </WContainer>
        );
    }

}


const styles = StyleSheet.create({

    btnStyle :{backgroundColor : Colors.btnColor,marginTop:50},
    registerStyle : {justifyContent:'center' ,flexDirection: 'row', marginVertical : 10 },
    textPr : {color:Colors.bBlack},
    textPb : {color:Colors.btnColor, marginLeft:5}

});