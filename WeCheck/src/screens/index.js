export { default as SplashScreen } from './Splash';
export { default as SignInScreen } from './SignIn';
export { default as SignUpScreen } from './SignUp';
export { default as HomeScreen } from './Home';
export { default as GenerarDocScreen } from './GenerarDoc';
export { default as DocumentosScreen } from './Documentos';
export { default as ValidarScreen } from './Validar';