import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    AsyncStorage,
    View,
    FlatList
} from 'react-native';
import moment from 'moment';
import {
    Text,
    CheckBox
} from 'native-base';
import { WButton, WInput, WContainer, WFContainer } from  '../components';
import { Colors, Storage, NavigationC, ServiceConfig } from '../constants';
import { DocumentoRequest, DropDownHolder } from '../model';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { DocumentoApi } from '../api';
import { NavigationUtil } from '../util';

export default class Screen extends React.Component {

    inicio = true;
    fechaInicioD = null;
    fechaFinD = null;

    state = {
        idTrabajador : null,
        documento : null,
        dni : '',
        nombreTrabajador : '',
        cargo : '',
        profesion : '',
        fechaInicio : '',
        fechaFin : '',
        isDateTimePickerVisible : false
    };

    componentWillMount(){

        let documento = this.props.navigation.getParam('documento', null );

        if(documento !== null){

            this.setState({ idTrabajador : 1 ,
                dni : documento.trabajador.numeroDocumento,
                nombreTrabajador : documento.trabajador.nombres,
                cargo : documento.cargo,
                profesion : documento.profesion,
                fechaInicio : documento.fechaInicio,
                fechaFin : documento.fechaFin,
            });

        }

    }

    generar(){

        let docReq = new DocumentoRequest({ dni : this.state.dni , nombreTrabajador : this.state.nombreTrabajador,
            cargo : this.state.cargo, profesion : this.state.profesion, fechaInicio : this.fechaInicioD,
            fechaFin : this.fechaFinD});

        let message = docReq.validate();

        if(message !== null){

            DropDownHolder.showAlert({type : DropDownHolder.TYPE_ERROR , title : 'Valores requeridos' , message});

        }else{

            DocumentoApi.generate({

                docReq,
                onSuccess : async (res) => {

                    NavigationUtil.navigateTo(NavigationC.APP_DOCUMENTOS, this.props);

                },
                onError : () => {

                    DropDownHolder.showAlert({type : DropDownHolder.TYPE_ERROR , title : 'No se pudo generar' , message : 'Error al generar el certificado.'});

                }

            });

        }

    }

    escogerFecha(date){


        let formattedDate = moment(date).format('DD/MM/YYYY');
        if(this.inicio){

            this.fechaInicioD = date;
            this.setState({fechaInicio : formattedDate, isDateTimePickerVisible : false });

        }else{

            this.fechaFinD = date;
            this.setState({fechaFin : formattedDate, isDateTimePickerVisible : false });

        }

    }

    render() {
        return (
            <WContainer form>
                <WFContainer>

                    <WInput placeholder={'DNI'}
                            value={this.state.dni}
                            keyboardType={'number-pad'}
                            onChangeText={(dni) => this.setState({dni}) }/>
                    <WInput placeholder={'Nombres y Apellidos'}
                            value={this.state.nombreTrabajador}
                            onChangeText={(nombreTrabajador) => this.setState({nombreTrabajador}) }/>
                    <WInput placeholder={'Cargo'}
                            value={this.state.cargo}
                            onChangeText={(cargo) => this.setState({cargo}) }/>
                    <WInput placeholder={'Titulo Profesional'}
                            value={this.state.profesion}
                            onChangeText={(profesion) => this.setState({profesion}) }/>
                    <WInput placeholder={'Fecha Inicio'}
                            iconName={'ios-calendar'}
                            value={this.state.fechaInicio}
                            editable = {false}
                            onPressIcon={() => {

                                this.setState({isDateTimePickerVisible:true});
                                this.inicio = true;

                            }}/>
                    <WInput placeholder={'Fecha Fin'}
                            iconName={'ios-calendar'}
                            value={this.state.fechaFin}
                            editable = {false}
                            onPressIcon={() => {

                                this.setState({isDateTimePickerVisible:true});
                                this.inicio = false;

                            }}/>


                    {this.state.idTrabajador === null &&
                    <WButton btnStyle={styles.btnStyle} text={'GENERAR'}
                             onPress={this.generar.bind(this)}/>
                    }

                    {this.state.idTrabajador &&
                    <WButton btnStyle={styles.btnStyle} text={'REGRESAR'}
                             onPress={()=> { this.props.navigation.goBack() }} />
                    }

                </WFContainer>
                <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this.escogerFecha.bind(this)}
                    onCancel={() => {this.setState({isDateTimePickerVisible: false})}}
                />
            </WContainer>
        );
    }

}


const styles = StyleSheet.create({

    btnStyle :{backgroundColor : Colors.btnColor,marginTop:50},
    registerStyle : {justifyContent:'center' ,flexDirection: 'row', marginVertical : 10 },
    textPr : {color:Colors.bBlack},
    textPb : {color:Colors.btnColor, marginLeft:5}

});
