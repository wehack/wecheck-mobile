import  React from 'react';
import { StyleSheet, View, AsyncStorage } from 'react-native';
import { WButton } from '../../components';
import { Grid , Col , Row} from 'native-base';
import { Colors, Storage, NavigationC } from '../../constants';
import { NavigationUtil } from "../../util";

class DrawerScreen extends React.Component {

    async logOut(){

        await AsyncStorage.removeItem(Storage.TOKEN);
        this.props.navigation.navigate(NavigationC.SPLASH)

    }

    render () {

        return (
            <View style={{flex:1,backgroundColor : Colors.baseColor}}>
                <Grid style={{margin:24 }}>
                    <Col size = {7}>

                        <WButton btnStyle={styles.btnStyle} text={'VALIDAR'}
                                 onPress={()=>{ this.props.navigation.navigate(NavigationC.APP_VALIDAR)}}/>

                        <WButton btnStyle={styles.btnStyle} text={'GENERAR DOC'}
                                 onPress={()=>{ this.props.navigation.navigate(NavigationC.APP_GENERAR_DOC)}}/>

                        <WButton btnStyle={styles.btnStyle} text={'DOCUMENTOS GENERADOS'}
                                 onPress={()=>{NavigationUtil.navigateTo(NavigationC.APP_DOCUMENTOS, this.props); }}/>
                    </Col>
                    <Row size={1}>

                        <WButton btnStyle={styles.btnStyle} text={'CERRAR SESIÓN'}
                                 onPress={this.logOut.bind(this)}/>

                    </Row>

                </Grid>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection:'column'
    },
    avatarContainer: {
        alignItems : 'center',
        paddingTop:25,
        marginTop:40

    },
    optionContainer:{
        alignItems:'center',
        paddingBottom:15,
        paddingTop:15
    },
    listOptions : {flex:9,marginTop:50   ,flexDirection:'column'},
    option : {flexDirection : 'column',marginBottom:20},
    btnsOptionContainer : {alignItems:'flex-end'},
    btnsOption : {flexDirection:'row'},
    userOptions : {flex:9,marginTop:50   ,flexDirection:'column'},
    appOptions : {flex:1,marginTop:50,flexDirection:'row'},
    btnAppOption : {flex:1,flexDirection:'column'},
    textAppOption : {color:'red',fontSize:13}
});
export default DrawerScreen;