import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    AsyncStorage,
    View,
    FlatList
} from 'react-native';
import {
    Text,
    CheckBox
} from 'native-base';
import { WButton, WInput, WContainer, WFContainer } from  '../components';
import { Colors, Storage, NavigationC, ServiceConfig } from '../constants';
import { AuthApi, UserApi } from '../api';
import { LoginRequest, DropDownHolder } from "../model";

export default class Screen extends React.Component {

    state = {
        empresa : true,
        documento : '',
        password : '',
        securePassword : true

    };

    componentWillMount(){

    }

    render() {
        return (
            <WContainer >
                <View style={{marginHorizontal:30}}>

                    <WButton btnStyle={styles.btnStyle} text={'VALIDAR'}
                             onPress={()=>{ this.props.navigation.navigate(NavigationC.APP_VALIDAR)}}/>

                    <WButton btnStyle={styles.btnStyle} text={'GENERAR DOC'}
                             onPress={()=>{ this.props.navigation.navigate(NavigationC.APP_GENERAR_DOC)}}/>

                </View>
            </WContainer>
        );
    }

}


const styles = StyleSheet.create({

    btnStyle :{backgroundColor : Colors.btnColor,marginTop:50},
    registerStyle : {justifyContent:'center' ,flexDirection: 'row', marginVertical : 10 },
    textPr : {color:Colors.bBlack},
    textPb : {color:Colors.btnColor, marginLeft:5}

});
