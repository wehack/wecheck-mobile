import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    AsyncStorage,
    View,
    FlatList
} from 'react-native';
import moment from 'moment';
import {
    Text,
    CheckBox
} from 'native-base';
import { WButton, WInput, WContainer, WFContainer } from  '../components';
import { Colors, Storage, NavigationC, ServiceConfig } from '../constants';
import { DocumentoRequest, DropDownHolder } from '../model';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { DocumentoApi } from '../api';

export default class Screen extends React.Component {


    state = {
        codigo : '',
        documentos : []
    };

    componentWillMount(){




    }

    validar(){

        DocumentoApi.validar({ codigo : this.state.codigo,
            onSuccess : async (res) => {

                //DropDownHolder.showAlert({type : DropDownHolder.TYPE_WARN , title : 'No hay documentos' , message : 'No se encontraron documentos.'});
                this.setState({documentos: res});

            },
            onError : () => {

                DropDownHolder.showAlert({type : DropDownHolder.TYPE_ERROR , title : 'No se pudo obtener documentos' , message : 'Error al obtener documentos.'});

            }

        });

    }

    render() {
        return (
            <WContainer>
                <View style={{flex:1,margin : 30}}>
                    <WInput placeholder={'CÓDIGO'}
                            value={this.state.codigo}
                            onChangeText={(codigo) => this.setState({codigo}) }/>
                    <WButton btnStyle={styles.btnStyle} text={'BUSCAR'}
                             onPress={this.validar.bind(this)} />
                    <FlatList
                        data={this.state.documentos}
                        keyExtractor={(item, index) => item.id.toString()}
                        renderItem={({item}) =>
                            <TouchableOpacity key={item.id} style={{backgroundColor:Colors.baseColor,flexDirection:'column', marginVertical:10,
                                padding:15,borderRadius:10,elevation:5}}
                                              onPress = { () => {

                                                  this.props.navigation.navigate( NavigationC.APP_GENERAR_DOC, {
                                                      documento : item
                                                  });

                                              }}>
                                <Text style={styles.item}>DNI : {item.trabajador.numeroDocumento}</Text>
                                <Text style={styles.item}>NOMBRES : {item.trabajador.nombres}</Text>
                                <Text style={styles.item}>CARGO : {item.cargo}</Text>
                                <Text style={styles.item}>PROFESION : {item.profesion}</Text>
                                <Text style={styles.item}>CÓDIGO : {item.codigoDocumento}</Text>
                            </TouchableOpacity>

                        }
                    />
                </View>
            </WContainer>
        );
    }

}


const styles = StyleSheet.create({

    btnStyle :{backgroundColor : Colors.btnColor,marginTop:50},
    registerStyle : {justifyContent:'center' ,flexDirection: 'row', marginVertical : 10 },
    textPr : {color:Colors.bBlack},
    textPb : {color:Colors.btnColor, marginLeft:5}

});
