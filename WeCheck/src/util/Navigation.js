import { StackActions, NavigationActions } from 'react-navigation';
import { NavigationC } from '../constants';

const navigateTo = (  routeName  = NavigationC.APP_MAIN, props ) => {
        
    const resetAction = StackActions.reset({
        index : 0,
        key : null,
        actions: [
            NavigationActions.navigate({ routeName })
        ]
    });
    
    props.navigation.dispatch(resetAction);

};

export default { navigateTo };
