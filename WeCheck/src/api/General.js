import { AsyncStorage } from 'react-native';
import { Storage, ServiceConfig } from '../constants';

const createHttpHeader = async (headers) => {

    let token = await AsyncStorage.getItem(Storage.TOKEN);
    let httpHeader = token ? {

        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`

    }: {'Content-Type': 'application/json'};
    if(headers){

        headers.forEach((option) =>{

            httpHeader[option.key] = option.value;

        });

    }
    return httpHeader;

};

const jfetch =  async (url,params , doActions = {onSuccess : () => {},onError : () => {},doAlways : () => {}}) =>{

    console.log(url);
    console.log(params.body);
    let method = params.method;
    let body = params.body;
    let headers = params.headers;
    let httpHeaders = await createHttpHeader(headers);
    let httpParams = method === 'POST' || method === 'PUT' ? {
        method : method,
        body : httpHeaders['Content-Type'] === 'x-www-form-urlencoded' ? body : JSON.stringify(body),
        headers : httpHeaders
    } : {method : method , headers : httpHeaders};
    console.log('Http Params : ' + JSON.stringify(httpParams));

    Promise.race([
        fetch(url, httpParams),
        new Promise( (resolve, reject) => {setTimeout(() =>{ reject(new Error('request timeout'))}, 20000)})
    ])
        .then((response) => response.json())
        .then((response)=>{

            console.log('DATA RESPONSE: ');
            console.log(response);
            doActions.doAlways();
            let error = response.error;

            if(error){

                doActions.onError(error);

            }else{

                if(response.code){

                    console.log('CODE');
                    console.log(response.code);
                    let dominio = response.dominio;
                    let code = response.code;

                    switch (code){

                        case ServiceConfig.CODIGO_ERROR_INTERNO:
                            doActions.onError(code);
                            break;

                        case ServiceConfig.CODIGO_NULL:
                            doActions.onSuccess(null);
                            break;

                        case ServiceConfig.CODIGO_LISTA_VACIA:
                            doActions.onSuccess([]);
                            break;

                        case ServiceConfig.CODIGO_OK:
                            doActions.onSuccess(dominio);
                            break;

                        case ServiceConfig.CODIGO_ERROR_AUTH:
                            doActions.onError(code);
                            break;

                        case ServiceConfig.CODIGO_USER_EXIST:
                            doActions.onError(code);
                            break;

                    }

                }else{

                    doActions.onSuccess(response);

                }

            }

        })
        .catch((error) => {

            console.log(error);

            if(doActions.doAlways)doActions.doAlways();

            if(doActions.onError)doActions.onError(error);

        });

};

export default {
    jfetch
};