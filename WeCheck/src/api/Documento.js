import { ServiceConfig } from '../constants';
import GeneralApi from './General'

const BASE_URL = `${ServiceConfig.BASE_URL_DEV}documento/`;
const GENERATE_URL  = `${BASE_URL}nuevo`;
const LIST_URL  = `${BASE_URL}listar`;
const VALIDAR_URL  = `${BASE_URL}buscarPorCodigo`;

const generate = ({docReq = {} ,onSuccess = () => {}, onError =  () => {}, doAlways = () => {}}) => {

    GeneralApi.jfetch(`${GENERATE_URL}`, {

        method: 'POST',
        body : docReq,

    }, { onSuccess , onError , doAlways });

};

const lista = ({onSuccess = () => {}, onError =  () => {}, doAlways = () => {}}) => {

    GeneralApi.jfetch(`${LIST_URL}`, {

        method: 'GET'

    }, { onSuccess , onError , doAlways });

};

const validar = ({codigo = null ,onSuccess = () => {}, onError =  () => {}, doAlways = () => {}}) => {

    GeneralApi.jfetch(`${VALIDAR_URL}?codigo=${codigo}`, {

        method: 'GET'

    }, { onSuccess , onError , doAlways });

};



export default {

    generate,
    lista,
    validar

};