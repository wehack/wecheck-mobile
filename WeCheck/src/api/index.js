export { default as AuthApi } from './Auth';
export { default as UserApi } from './User';
export { default as DocumentoApi } from './Documento';