import { ServiceConfig } from '../constants';
import GeneralApi from './General'

const BASE_URL = `${ServiceConfig.BASE_URL_DEV}api/`;
const BASE_URL2 = `${ServiceConfig.BASE_URL_DEV}auth/`;
const LOGIN_URL  = `${BASE_URL}login`;
const SIGN_UP_URL  = `${BASE_URL2}nuevo`;


const signIn = ({ loginReq, onSuccess = () => {}, onError =  () => {}, doAlways = () => {}}) => {

    GeneralApi.jfetch(`${LOGIN_URL}?username=${loginReq.username}&password=${loginReq.password}&empresa=${loginReq.empresa}`, {

        method: 'POST',
        body : {},
        headers : [ { key : 'Content-Type' , value :  'application/x-www-form-urlencoded;charset=UTF-8' }]

    }, { onSuccess , onError , doAlways });


};

const signUp = ({ signUpReq, onSuccess = () => {}, onError =  () => {}, doAlways = () => {}}) => {

    GeneralApi.jfetch(`${SIGN_UP_URL}`, {

        method: 'POST',
        body : signUpReq

    }, { onSuccess , onError , doAlways });

};


export default {

    signIn,
    signUp

};