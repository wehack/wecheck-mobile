import { ServiceConfig } from '../constants';
import GeneralApi from './General'

const BASE_URL = `${ServiceConfig.BASE_URL_DEV}user/`;
const GET_CURRENT_URL  = `${BASE_URL}me`;


const getCurrentUser = ({ onSuccess = () => {}, onError =  () => {}, doAlways = () => {}}) => {

    GeneralApi.jfetch(`${GET_CURRENT_URL}`, {

        method: 'GET'

    }, { onSuccess , onError , doAlways });

};


export default {

    getCurrentUser

};