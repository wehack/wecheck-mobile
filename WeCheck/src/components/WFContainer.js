import React from 'react';
import {
    StyleSheet,
} from 'react-native';
import {
    Grid,
    Col,
    Content
} from 'native-base';
import Colors from '../constants/Colors';

export default class Screen extends React.Component {

    render() {
        return (
            <Grid style={styles.gridStyle}>
                <Content style={styles.contenStyle}>
                    <Col style = {styles.formStyle}>
                        {this.props.children}
                    </Col>
                </Content>
            </Grid>
        );
    }

}

const styles = StyleSheet.create({

    gridStyle : {
        alignItems:'center',
        marginHorizontal:15,
    },
    formStyle : {
        backgroundColor:Colors.bWhite,
        justifyContent: 'center',
        alignContent:'center',
        paddingHorizontal:12,
        paddingVertical:30,
        borderRadius : 10,
    },
    contenStyle :{
        elevation:5,
        borderRadius : 10,
    }

});
