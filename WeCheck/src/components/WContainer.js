import React from 'react';
import Spinner from 'react-native-loading-spinner-overlay';
import {
    StyleSheet,
} from 'react-native';
import {
    Container,
    Grid,
    Col,
    Row
} from 'native-base';
import Colors from '../constants/Colors';
import Layout from '../constants/Layout';
import DropdownAlert from "react-native-dropdownalert";
import { DropDownHolder } from "../model";

export default class Screen extends React.Component {

    render() {
        return (
            <Container>
                {this.props.form !== undefined &&
                    <Grid style={styles.formStyle}>
                        <Col>
                            <Row size= { 4 } style={{backgroundColor : Colors.baseColor}} />
                            <Row size= { 6 } style={{backgroundColor : Colors.bWhite}} />
                        </Col>
                    </Grid>
                }

                {this.props.iform !== undefined &&
                    <Grid style={styles.baseStyle}>
                        <Col>
                            <Row size= { 6 } style={{backgroundColor : Colors.bWhite}} />
                            <Row size= { 4 } style={{backgroundColor : Colors.baseColor}} />
                        </Col>
                    </Grid>
                }
                {this.props.children}
                <Spinner visible={this.props.isLoading}/>
                <DropdownAlert ref={ref => DropDownHolder.setDropDown(ref)} closeInterval={6000}/>
            </Container>
        );
    }

}


const styles = StyleSheet.create({

    baseStyle : {position:'absolute',width: Layout.window.width, height: Layout.window.height},
    formStyle : {position:'absolute',width: Layout.window.width, height: Layout.window.height}

});