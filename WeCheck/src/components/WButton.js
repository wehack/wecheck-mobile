import React from 'react';
import {
    StyleSheet,
    TouchableOpacity
} from 'react-native';
import {
    Text,
    Icon,
    Thumbnail
} from 'native-base';
import { Colors, Fonts } from '../constants';

const styles = StyleSheet.create({

    btnStyle : {
        elevation: 2,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor : Colors.fbBColor,
        flexDirection:'row'
    },
    btnBorderRStyle : {
        borderRadius : 15
    },
    btnLeftBorderRStyle : {
        borderTopLeftRadius : 10,
        borderBottomLeftRadius: 10
    },
    btnRigthBorderRStyle : {
        borderTopRightRadius : 10,
        borderBottomRightRadius : 10
    },
    btnPadding: {
        paddingHorizontal: 30 ,
        paddingVertical: 15
    },
    btnMargin:{
        marginVertical: 7,
    },
    txtStyle : {
        color:Colors.bWhite
    }

});

const component = ({ btnStyle={}, txtStyle = {} , text = '', onPress = () =>{}, iconName = '', imageSrc = '',
                       noBorderRadius, noPadding, noMargin, lBorderRadius, rBorderRadius }) => {

    return (
        <TouchableOpacity style={[styles.btnStyle,
                            noBorderRadius || lBorderRadius || rBorderRadius ? {} : styles.btnBorderRStyle,
                            lBorderRadius ? styles.btnLeftBorderRStyle : {},
                            rBorderRadius ? styles.btnRigthBorderRStyle : {},
                            noPadding ?{} : styles.btnPadding,
                            noMargin? {}:styles.btnMargin,
                            btnStyle]}
                          onPress={() => onPress()}>
            {iconName !== '' &&
                <Icon name={iconName} style={{color: Colors.bWhite}}> </Icon>
            }
            {imageSrc !== '' &&
                <Thumbnail small source={{uri : imageSrc}} />
            }
            <Text style={[styles.txtStyle,txtStyle]}>{text}</Text>
        </TouchableOpacity>
    );
};

export default component;