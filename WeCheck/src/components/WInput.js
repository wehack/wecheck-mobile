import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    View,
    TextInput
} from 'react-native';
import {
    Icon
} from 'native-base';
import { Colors, Fonts } from '../constants';

const styles = StyleSheet.create({

    contStyle : {
        backgroundColor:Colors.bGray,
        flexDirection:'row'
    },
    contBorderRStyle : {
        borderRadius : 10,
    },
    contLeftBorderRStyle : {
        borderTopLeftRadius : 10,
        borderBottomLeftRadius: 10
    },
    contRigthBorderRStyle : {
        borderTopRightRadius : 10,
        borderBottomRightRadius : 10
    },
    contPadding: {
        paddingHorizontal: 15 ,
        paddingVertical: 5,
    },
    contMargin:{
        marginVertical: 7,
    },
    inpStyle : {
        color:Colors.bBlack,
        flex:9
    },
    touchIconStyle:{
        flex:1,
        justifyContent:'center'
    },
    iconStyle : {
        color: Colors.bBlack2,
    }

});

const component = ({ contStyle ={}, inpStyle = {}, iconStyle = {}, placeholder = '', placeholderColor =Colors.bBlack2,
                       onPressIcon = () =>{}, onChangeText = () => {}, iconName = '', value = '', editable = true,
                       keyboardType = 'default', returnKeyType = 'done',
                       onBlur = () => {}, secureTextEntry = false,
                       noBorderRadius, lBorderRadius, rBorderRadius, noPadding, noMargin }) => {

    return (
        <View style={[styles.contStyle,
            noBorderRadius || lBorderRadius || rBorderRadius ? {} : styles.contBorderRStyle,
            lBorderRadius ? styles.contLeftBorderRStyle : {},
            rBorderRadius ? styles.contRigthBorderRStyle : {},
            noPadding ?{} : styles.contPadding,
            noMargin? {}:styles.contMargin,
            contStyle]}>
            <TextInput placeholder={placeholder}
                       placeholderTextColor={placeholderColor}
                       style={[styles.inpStyle,inpStyle]}
                       onChangeText={(val) => onChangeText(val)}
                       onBlur={(val) => onBlur(val)}
                       value={value}
                       editable={editable}
                       keyboardType={keyboardType}
                       returnKeyType={returnKeyType}
                       secureTextEntry={secureTextEntry}
            />
            {iconName !== '' &&
                <TouchableOpacity  onPress={() => onPressIcon()} style={styles.touchIconStyle}>
                    <Icon name={iconName} style={[styles.iconStyle,iconStyle]}> </Icon>
                </TouchableOpacity>
            }
            {

            }

        </View>
    );
};

export default component;