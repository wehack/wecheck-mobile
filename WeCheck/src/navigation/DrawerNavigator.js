import { createDrawerNavigator, createStackNavigator } from 'react-navigation';
import { GenerarDocScreen , HomeScreen, DocumentosScreen, ValidarScreen } from '../screens'
import { Drawer } from '../screens/common'

const MainStack = createStackNavigator({

    Main : {
        screen : HomeScreen
    },
    GenerarDoc : {
        screen : GenerarDocScreen
    },
    Documentos : {
        screen : DocumentosScreen
    },
    Validar : {
        screen : ValidarScreen
    }

},{
    headerMode: 'none'
});

export default createDrawerNavigator(
    {       
        MainStack :{ 
            screen: MainStack
        }
    },
    {
        contentComponent: Drawer
    }
);