import { createSwitchNavigator, createStackNavigator, createAppContainer } from 'react-navigation';
import DrawerNavigator from './DrawerNavigator';
import { SignInScreen, SignUpScreen, SplashScreen } from '../screens';

const AuthStack = createStackNavigator({ SignIn: SignInScreen , SignUp: SignUpScreen},{
    headerMode: 'none'
});

export default createAppContainer(createSwitchNavigator({
  Splash : SplashScreen,
  App : DrawerNavigator ,
  Auth : AuthStack
}));