export default {

  bGray : '#f0f0f0',
  bBlack : '#434343',
  bBlack2 : '#979797',
  btnColor : '#3b5998',
  fbBColor : '#3b5998',
  gmBColor : '#D44638',
  baseColor : '#00BCD4',
  bWhite : '#ffffff',

};
