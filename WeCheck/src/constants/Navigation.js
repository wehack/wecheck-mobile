export default {

    APP : 'App',
    APP_MAIN : 'Main',
    APP_GENERAR_DOC : 'GenerarDoc',
    APP_DOCUMENTOS : 'Documentos',
    APP_VALIDAR : 'Validar',
    AUTH : 'Auth',
    AUTH_SIGN_IN : 'SignIn',
    AUTH_SIGN_UP : 'SignUp',
    SPLASH : 'Splash'

}